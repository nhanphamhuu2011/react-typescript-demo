FROM node:14.16.1-alpine
 
# set working directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
 
# add 'PATH /usr/src/app/node_modules/.bin' to $PATH
ENV PATH /usr/src/app/node_modules/.bin:$PATH
 
# install and cache app dependencies
COPY package*.json /usr/src/app/
RUN npm install --silent
 
CMD ["npm", "start"]